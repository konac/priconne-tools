 **# priconneTools** 

#### 介绍
priconnent Redive B服自用工具集，仅作个人学习使用，不用于商业用途。GPL

1.pjjc的对战记录保存，查询。

2.会战自动点击。

#### DD me

* author 玩家id:1 069 965 843 583
* 1661530722@qq.com

#### 使用说明
> pjjcTool
v0_9.0

*pjjc的对战记录保存，查询。配合作业网使用。
*pjjc工具通过ocr识别截图玩家名称，保存玩家名称和截图文件到result.txt中，需要paddleocr的库，这个库的中文识别不错，不过有一些生僻字和符号可能识别不出来。

1) 将截图放入temp中，
2) 进入cmd，cd进入pjjcTool文件夹
3) 执行指令“python cutPic.py”和"python orcName.py"
4) 查看result结果

> clicksim
V0_6.0
*会战自动点击，需要adb，根据指定序列模拟点击事件。
* 目前采用的是最简单的文本文件输入
* "2 6"代表等待6秒，(6可以换成小数
* "1 5"表示点击5号位置

#### 哔哔
> pjjcTool的todo：
#通过api获取对战记录，查询作业 （

#图像map到角色（

#用一个可持久化的数据结构保存以上结果？（并不清楚sql和持久化数据结构的关系

> clicksim的todo:

#以后可能改为用xml配置，

#加入配置1，2，3，4，5角色的UB帧数，以便转化时间和帧数。

#通过连点在载入时卡时间1：30或者剩余时间？

#### end