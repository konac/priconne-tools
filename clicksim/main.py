import os
import time


def adb_click(center, offset=(0, 0)):
    (x, y) = center
    x += offset[0]
    y += offset[1]
    os.system(f"adb shell input tap {x} {y}")
    return


# os.system('adb devices')

Pos_battle_start = (1366,750)
Pos_ch5 = (400,750)
Pos_ch4 = (600,750)
Pos_ch3 = (800,750)
Pos_ch2 = (1000,750)
Pos_ch1 = (1200,750)
Pos_auto = (1520,700)
P_menu = (1500,40)
P_back = (550,725)
L_Pos_ch = [Pos_auto, Pos_ch1, Pos_ch2, Pos_ch3, Pos_ch4, Pos_ch5, P_back]

with(open('setting.txt')) as f:
    for line in f:
        if line[0] == '1':
            wt = float(line[2:])
            time.sleep(wt)
        elif line[0] == '2':
            adb_click(L_Pos_ch[int(line[2])])
