import os
from PIL import Image

def image_cut_save(path, left, upper, right, lower, save_path):
    """
        所截区域图片保存
    :param path: 图片路径
    :param left: 区块左上角位置的像素点离图片左边界的距离
    :param upper：区块左上角位置的像素点离图片上边界的距离
    :param right：区块右下角位置的像素点离图片左边界的距离
    :param lower：区块右下角位置的像素点离图片上边界的距离
     故需满足：lower > upper、right > left
    :param save_path: 所截图片保存位置
    """
    img = Image.open(path)  # 打开图像
    box = (left, upper, right, lower)
    roi = img.crop(box)
    # 保存截取的图片
    roi.save(save_path)


path = os.getcwd()+'\\temp'

for f_name in os.listdir(path):
    # if fnmatch.fnmatch(f_name, 'Screenshot*.jpg'):
    if f_name.startswith('Screenshot'):
        if(f_name.endswith('.jpg')):
            f_name2 = f_name[11:26]
            # print(f_name2)
            d_path = path + '//' + f_name2 + '.jpg'
            if not os.path.isfile(d_path):
                s_path = path + '//' + f_name
                left, upper, right, lower = 1150, 178, 1533, 212
                image_cut_save(s_path, left, upper, right, lower, d_path)
        elif(f_name.endswith('.png')):
            f_name2 = f_name[11:26]
            d_path = path + '//' + f_name2 + '.png'
            if not os.path.isfile(d_path):
                s_path = path + '//' + f_name
                left, upper, right, lower = 760, 148, 1075, 179
                image_cut_save(s_path, left, upper, right, lower, d_path)