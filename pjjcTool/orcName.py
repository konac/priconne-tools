import os
from paddleocr import PaddleOCR


path = os.getcwd()+'\\temp'
ocr = PaddleOCR(use_angle_cls=True, lang="ch")

with open("./result.txt", 'a') as out_file:
	for f_name in os.listdir(path):
		if not f_name.startswith('Screenshot'):
			img = path + '//' + f_name
			result = ocr.ocr(img, cls=True)
			if result[0]:
				text = result[0][0][1][0]
			else:
				text = ""
			if f_name.endswith('.jpg'):
				outlines = text+"\nScreenshot_"+f_name[:-4]+"_com.bilibili.priconne.jpg\n"
			elif f_name.endswith('.png'):
				outlines = text+"\nScreenshot_"+f_name+"\n"
			out_file.write(outlines)